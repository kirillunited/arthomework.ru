var gulp = require('gulp'),
    rigger = require('gulp-rigger'),
    del = require('del'),
    rename = require('gulp-rename');

var path = {
    src: {
        html: 'src/template/*.html',
        js: 'src/js/*.js'
    },
    build: {
        html: 'build',
        js: 'build/assets/js'
    }
}

gulp.task('inject', function () {
    var htmlInject = gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(rename(function (path) {
            path.basename = 'index';
        }))
        .pipe(gulp.dest(path.build.html));

    var jsInject = gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js));
});

gulp.task('clean', function () {
    return del.sync('build'); // Удаляем папку перед сборкой
});

gulp.task('watch', ['inject'], function () {
    gulp.watch('src/**/*', ['inject']);
});

gulp.task('clear', function () {
    return cache.clearAll();
})

gulp.task('default', ['watch']);