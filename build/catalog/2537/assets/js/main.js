$(document).ready(function () {
    var iconRub = '<i class="fa fa-rub" aria-hidden="true"></i>';
    var iconSearch = '<i class="fa fa-search" aria-hidden="true"></i>';
    var iconPhone = '<i class="fa fa-phone" aria-hidden="true"></i>';
    var iconClose = '<i class="fa fa-times" aria-hidden="true"></i>'
    $(iconRub).appendTo('.icon-rub');
    $(iconPhone).appendTo('.icon-phone');
    $(iconSearch).appendTo('.icon-search');
    $(iconClose).appendTo('.close.button');
    $('.icon-rub, .icon-phone, .icon-search, .close.button').addClass('hide-ico');
});