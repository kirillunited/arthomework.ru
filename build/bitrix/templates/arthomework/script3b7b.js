$(function () {
    var startSlide = 1;
    if (window.location.hash) {
        startSlide = window.location.hash.replace('#', '');
    }
    $('#slides').slides({
        preload: true,
        preloadImage: 'img/loading.gif',
        generatePagination: true,
        play: 5000, //~!!!
        pause: 2500, //~!!!
        hoverPause: true,
        start: startSlide,
        //animationComplete: function(current){
        //window.location.hash = '#' + current;
        //	}
    });
});
$(function () {

    var countli = $(".caroufredsel_wrapper ul li").size()
    if (countli < 4) {
        $(".caroufredsel_wrapper").find(".next").addClass("disable")
    }

    $(".close").live('click', function () {
        CloseModalWindow()
    });
    $(document).keyup(function (event) {
        if (event.keyCode == 27) {
            CloseModalWindow()
        }
    });

});

function CentriredModalWindow(ModalName) {
    $(ModalName).css({"display": "block", "opacity": 0});
    var modalH = $(ModalName).height();
    var modalW = $(ModalName).width();
    $(ModalName).css({
        "margin-left": "-" + (parseInt(modalW) / 2) + "px",
        "margin-top": "-" + (parseInt(modalH) / 2) + "px"
    })
}

function OpenModalWindow(ModalName) {
    $(ModalName).animate({"opacity": 1}, 300);
    $("#bgmod").css("display", "block");
}

function CloseModalWindow() {
    $("#bgmod").css("display", "none");
    $(".modal").css({"opacity": 1});
    $(".modal").animate({"opacity": 0}, 300);
    setTimeout(function () {
        $(".modal").css({"display": "none"});
    }, 500)
}

function addToCompare(element, mode, text, deleteUrl) {
    if (!element && !element.href) return;

    var href = element.href;
    var button = $(element);

    button/*.unbind('click')*/.removeAttr("href");
    titleItem = $(element).parents(".R2D2").find(".item_title").attr('title')
    imgItem = $(element).parents(".R2D2").find(".item_img").attr('src');
    $('#addItemInCompare .item_title').text(titleItem);
    $('#addItemInCompare .item_img img').attr('src', imgItem);
    var ModalName = $('#addItemInCompare');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName);

    if (mode == 'list') {
        var removeCompare = '<input type="checkbox" class="addtoCompareCheckbox"/ checked><span class="checkbox_text">' + text + '</span>';
        button.html(removeCompare);
        button.attr("href", deleteUrl);
        button.attr("onclick", "return deleteFromCompare(this, \'" + mode + "\', \'" + text + "\', \'" + href + "\');");
    }
    else if (mode == 'detail' || mode == 'list_price') {
        var removeCompare = text;
        //button.html(removeCompare);
    }

    if (href)
        $.get(href + '&ajax_compare=1&backurl=' + decodeURIComponent(window.location.pathname),
            $.proxy(
                function (data) {
                    var compare = $("#compare");
                    compare.html(data);

                    if (compare.css("display") == "none") {
                        compare.css({"display": "block", "height": "0"});
                    }
                }, button
            )
        );
    return false;
}

function deleteFromCompare(element, mode, text, compareUrl) {
    if (!element || !element.href) return;

    //var href = element.href;
    var button = $(element);
    var href = button.attr("href");

    button.unbind('click').removeAttr("href");
    if (mode == 'list') {
        var removeCompare = '<input type="checkbox" class="addtoCompareCheckbox"/><span class="checkbox_text">' + text + '</span>';
        button.html(removeCompare);
        button.attr("href", compareUrl);
        button.attr("onclick", "return addToCompare(this, \'" + mode + "\', \'" + text + "\', \'" + href + "\');");
    }

    $.get(href + '&ajax_compare=1&backurl=' + decodeURIComponent(window.location.pathname),
        $.proxy(
            function (data) {
                var compare = $("#compare");
                compare.html(data);
                if (compare.css("display") == "none") {
                    compare.css({"display": "block", "height": "0"});
                }
            }, button
        )
    );
    return false;
}

function disableAddToCompare(element, mode, text, deleteUrl) {
    if (!element) return;

    element = $(element);
    var href = element.attr("href");
    if (mode == 'list') {
        var removeCompare = '<input type="checkbox" class="addtoCompareCheckbox"/ checked><span class="checkbox_text">' + text + '</span>';
        element.html(removeCompare);
        element.attr("onclick", "return deleteFromCompare(this, \'" + mode + "\', \'" + text + "\', \'" + href + "\');");
        element.attr("href", deleteUrl);
    }
    else if (mode == 'detail' || mode == 'list_price') {
        var removeCompare = text;
        element
        //.html(removeCompare)
            .unbind('click').removeAttr("href");
    }
    return false;
}

function compareTable(counttd) {
    $(".table_compare").find("table").css("width", 100 + "%")
    $(".table_compare table tr:first").find("td").css("width", "auto");
}

function deleteFromCompareTable(element) {
    var counttd = $(".table_compare table tr:first").find("td img").size()
    if (counttd == 1) {
        var heightsum = $(".workarea").height() - $(".breadcrumbs").height()
        $(".table_compare").remove();
        $(".filtren.compare").remove();
        $(".deleteAllFromCompareLink").remove();
        $(".emptyListCompare").css("display", "block");
        $(".sort").remove();
        $(".nocomapre").css({"height": heightsum + "px", "display": "table", "width": 100 + "%"}, 300);

    } else if (counttd < 4) {
        compareTable(counttd);
    }
    var tdInd = $(element).parent('td').index();
    wtdc = $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").width()
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").css({
        "width": wtdc + "px",
        "padding": 0 + "px"
    });
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").animate({
        "width": 0 + "px",
        "padding": 0 + "px"
    }, 300);
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").text('');
    $(element).parents('table').find("tr").find("td:nth(" + tdInd + ")").remove();

    var href = element.href;
    $.get(href);

    return false;
}

function showFavsLink(elem) {

}

function quickOrderForm(element, mode) {
    var button = $(element);

    if (mode == "buy1") {
        var idd = $(element).attr('data-elem-id');
        titleItem = button.parents(".R2D2").find(".item_title").attr('title');
        imgItem = button.parents(".R2D2").find(".item_img").attr('src');
        $('#quick-order .item_title').text(titleItem);
        var quantity = button.parent().find('#QUANTITY_').val();
        $('#quick-order #buy_one_click').attr('data-quantity', quantity);
    }

    $('#quick-order #buy_one_click').attr('data-buymode', mode);

    var ModalName = $('#quick-order');
    $('#quick-order .phone').mask("+7 (999) 999-99-99");
    CloseModalWindow();
    setTimeout(function () {
        CentriredModalWindow(ModalName);
        OpenModalWindow(ModalName);
    }, 700)
}

function showFlyAuthForm() {
    var modalH = $("#login").height();
    $("#login").css({"display": "block", "margin-top": "-" + (parseInt(modalH) / 2) + "px", "opacity": "1"});
    return false;
}

function quickOrderFormByElem(btn, titleItem, id) {

    $('#quick-order .item_title').text(titleItem);
    $('#quick-order #buy_one_click').attr('data-elem-id', id);
    $('#quick-order #buy_one_click').attr('data-buyMode', "buy1");
    var quantity = $(btn).parent().find('.qua_input').val();
    $('#quick-order #buy_one_click').attr('data-quantity', quantity);

    var ModalName = $('#quick-order');
    $('#quick-order .phone').mask("+7 (999) 999-99-99");
    CloseModalWindow();
    setTimeout(function () {
        CentriredModalWindow(ModalName);
        OpenModalWindow(ModalName);
    }, 700)
}

function rememberPassForm(phone) {
    var ModalName = $('#remember-auth-by-phone');
    $('#remember-auth-by-phone .phone').mask("+7 (999) 999-99-99");
    $('#remember-auth-by-phone .phone').val(phone);
    CloseModalWindow();
    setTimeout(function () {
        CentriredModalWindow(ModalName);
        OpenModalWindow(ModalName);
    }, 700)
}

function rememberPass(elem) {
    var parent = $(elem).parent();
    var phone = parent.find('.phone').val();
    $.ajax({
        type: 'POST',
        data: {phone: phone},
        url: '/ajax/rememberpassphone.php',
        success: function (data) {
            var result = JSON.parse(data);
            if (result.SUCCESS == true) {
                $('#remember-auth-by-phone .error-block').html("�� ��� ������� ������� ������� ������� ������.");
            } else {
                if (result.CODE == 5) {
                    $('#remember-auth-by-phone .error-block').html("������������ � ����� ���������� ������� �� ����������.");
                } else {
                    $('#remember-auth-by-phone .error-block').html("��������� ������. ���������� �������.");
                }
            }
        }
    });
}

function addFavorites(elem) {
    var id = $(elem).attr('data-id');
    $(elem).attr('title', '������� �� ����������');

    $.ajax({
        type: 'POST',
        data: {elemId: id, type: 'setFav'},
        url: '/ajax/favorites.php',
        success: function (data) {
            if (data) {
                $(elem).parent('.block-fav').addClass('in-favs');
                $(elem).attr('title', '������� �� ����������');
            } else {
                $(elem).parent('.block-fav').removeClass('in-favs');
                $(elem).attr('title', '�������� � ���������');
                if ($('body div.favorites').length) {
                    $(elem).parents('.R2D2').remove();
                }
            }
        }
    });
}

function markFavorites() {
    $.ajax({
        type: 'GET',
        data: {type: 'getFavs'},
        url: '/ajax/favorites.php',
        success: function (data) {
            if (data) {
                try {
                    var elemsId = JSON.parse(data);
                    elemsId.forEach(function (item, i, arr) {
                        $('.btnFavorites[data-id=' + item + ']').parent('.block-fav').addClass('in-favs');
                        $('.btnFavorites[data-id=' + item + ']').attr('title', '������� �� ����������');
                    });

                    if (elemsId.length>0) {
                        $('.d_kvad_tab.fa').addClass('favs-marked');
                    }

                } catch (e) {

                }
            } else {

            }
        }
    });
}

function quickOrder(element) {
    var parent = $(element).parents('.content');
    var itemId = $(element).attr('data-elem-id');
    var buyMode = $(element).attr('data-buymode');
    var quantity = $(element).attr('data-quantity');
    var name = parent.find('.name').val();
    var phone = parent.find('.phone').val();
    var email = parent.find('.email').val();
    var comment = parent.find('.comment').val();
    if (parent.find('.consent').length && parent.find('.consent input').attr("checked") != "checked") {
        parent.find('.error-block').html('<p style="color: red">�� ������� ��������!</a>');
        return;
    }
    var data = {
        name: name,
        phone: phone,
        prodId: itemId,
        email: email,
        comment: comment,
        buyMode: buyMode,
        itemQuantity: quantity
    };
    console.info(data);
    $.ajax({
        type: 'POST',
        data: data,
        url: '/ajax/oneclickbuy.php',
        success: function (data) {
			console.info(data);
            var result = JSON.parse(data);

            if (result.SUCCESS === true) {
                if (result.CODE == 1) {
                    document.location = "/personal/order/make/?ORDER_ID="+result.ORDER_ID;
                    parent.html('�������, ��� ����� ������.<br>�������� ������ ���������!<br>�� ��� ������� ����� ���, ���������� ����� � ������ ��� ����������� �� �����.');
                } else {
                    document.location = "/personal/order/make/?ORDER_ID="+result.ORDER_ID;
                    parent.html('�������, ��� ����� ������.<br>�������� ������ ���������!');
                }
            } else {
                switch (result.CODE) {
                    case 1:
                        parent.find('.error-block').html('<p style="color: red">���������� � ����� ��������� ��� ���������������.<br>��� ���������� <a href="/login/">��������������!</a></p><a onclick="rememberPassForm(\'' + phone + '\')">����� ����� �/��� ������</a>');
                        break;
                    case 11:
                        parent.find('.error-block').html('�� ������� ���!');
                        break;
                    case 12:
                        parent.find('.error-block').html('�� ������ �������!');
                        break;
                    case 13:
                        parent.find('.error-block').html('�� ��������� ������ EMail!');
                        break;
                    case 14:
                        parent.find('.error-block').html('� ����� ������� ��� �������, ������� �������� ����������. �������� �������� �/��� ��������� � �������������� �����!');
                        break;
                    default:
                        parent.find('.error-block').html('���������� ������. ��������� � �������������� �����.');
                        console.info(result);
                        break;
                }


            }
        }
    });
}

function addToCart(element, mode, text, type, quan) {
    if (!element && !element.href)
        return;

    var href = element.href;
    var button = $(element);
    var idd = element.id.replace(/catalog_add2cart_link_/g, "");
    $('#addItemInCart #QUA_VAL').val(idd);


    button.unbind('click').removeAttr("href");

    var titleItem = button.parents(".R2D2").find(".item_title").attr('title');
    var imgItem = button.parents(".R2D2").find(".item_img").attr('src');
    if (imgItem === undefined) {
        imgItem = button.parents(".R2D2").find(".item_img").css('background-image');
        imgItem = imgItem.replace(/(url\(|\)|'|")/gi, '');
    }
    $('#addItemInCart .item_title').text(titleItem);
    $('#addItemInCart .item_img img').attr('src', imgItem);

    var ModalName = $('#addItemInCart');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName);
    $('#quick-order .item_title').text(titleItem);
    $('#quick-order #buy_one_click').attr('data-elem-id', idd);
    $('#quick-order #buy_one_click').attr('data-buyMode', "buy1");

    var dop_q = "";
    if (quan !== undefined) {
        dop_q = "&quantity=" + quan;
    }
    if (href) {$.get(href + "&ajax_buy=1" + dop_q)}
    //$("#cart_line a").html(' '+(parseInt($("#cart_line a").html())+parseInt(quan)));
    //quantity=
    /*if (href) {
        $.get(href + "&ajax_buy=1" + dop_q,
            function (data) {
                $("#cart_line").html(data);
                $('#addItemInCart #detaili_bsket').load('/ajax/detaili_bsket.php?ELEMENT_ID=all');
                $.get('/ajax/detaili_bsket.php?ELEMENT_ID=' + idd, function (data) {
                    if ($('#addItemInCart #QUANTITY_').length>0) {
                        $('#addItemInCart #QUANTITY_').val(data);
                    }
                });
            }
        );
    }*/
    $('#addItemInCart #detaili_bsket').load('/ajax/detaili_bsket.php?ELEMENT_ID=all');
    $('#addItemInCart #QUANTITY_').val(quan);

    return false;
}

function disableAddToCart(elementId, mode, text) {
    var element = $("#" + elementId);
    if (!element)
        return;

    $(element).removeAttr("href");
    /*if (mode == "detail")
     $(element).html(text).removeAttr("href").unbind('click').css("cursor", "default").removeClass("addtoCart").addClass("incart");
     else if (mode == "list")
     $(element).html(text).unbind('click').css("cursor", "default").removeAttr("href").removeClass("addtoCart").addClass("incart");
     else if (mode == "detail_short")
     $(element).html(text).unbind('click').css("cursor", "default").removeAttr("href"); */
}

function DeleteFromCart(element) {
// $(element).parents('tr').remove();
// setTimeout(function(element) { $(element).parents('tr').remove() }, 300)
    $(element).parents('tr').animate({"height": 0 + "px", "opacity": 0, "overflow": "hidden", "display": "none"}, 300);
    $(element).parents('tr').find("td").animate({"height": 0 + "px", "padding": 0 + "px"}, 300);
    $(element).parents('tr').find("td").text('').remove();
    var href = element.href;
    if (href)
        $.get(href);

    return false;
}

function addToSubscribe(element, text) {

    var button = $(element);

    var href = element.href;
    var titleItem = $(element).parents(".R2D2").find(".item_title").attr('title')
    var imgItem = $(element).parents(".R2D2").find(".item_img").attr('src');
    if (imgItem === undefined) {
        imgItem = button.parents(".R2D2").find(".item_img").css('background-image');
        imgItem = imgItem.replace(/(url\(|\)|'|")/gi, '');
    }
    $('#addItemInSubscribe .item_title').text(titleItem);
    $('#addItemInSubscribe .item_img img').attr('src', imgItem);
    var ModalName = $('#addItemInSubscribe');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName)

    //$(element).html(text).addClass("incart");
    if (href)
        $.get(href, function () {
        });

    return false;
}

function addOfferToSubscribe(element, text, mode) {
    var href = element.href;
    if (mode == 'list') {
        $('#addItemInCartOptions').css({"display": "none"});
        titleItem = $("#addItemInCartOptions").find(".item_title").attr('title');
        imgItem = $("#addItemInCartOptions").find(".item_img img").attr('src');
    }
    else if (mode == 'detail') {
        titleItem = $(".R2D2").find(".item_title").attr('title');
        imgItem = $(".R2D2").find(".item_img").attr('src');
    }
    $('#addItemInSubscribe .item_title').text(titleItem);
    $('#addItemInSubscribe .item_img img').attr('src', imgItem);
    var ModalName = $('#addItemInSubscribe');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName)

//	$(element).html(text).addClass("incart");
    if (href)
        $.get(href, function () {
        });

    return false;
}

function disableAddToSubscribe(elementId, text) {
    $("#" + elementId)/*.html(text).addClass("incart")*/.unbind('click').removeAttr("href");
}

//SKU
function addOfferToCart(element, mode, text) {
    if (!element && !element.href)
        return;

    var button = $(element);

    $('#addItemInCartOptions').css({"display": "none"});
    titleItem = $("#addItemInCartOptions").find(".item_title").attr('title');
    imgItem = $("#addItemInCartOptions").find(".item_img img").attr('src');
    $('#addItemInCart .item_title').text(titleItem);
    $('#addItemInCart .item_img img').attr('src', imgItem);
    var ModalName = $('#addItemInCart');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName)

    if (element.href)
        $.get(element.href + "&ajax_buy=1", function (data) {
                $("#cart_line").html(data);
            }
        );
    return false;
}

function addOfferToCompare(element, mode, text) {
    if (!element || !element.href) return;

    var href = element.href;
    var button = $(element);

    $('#addItemInCartOptions').css({"display": "none"});
    button.unbind('click').removeAttr("href");
    titleItem = $("#addItemInCartOptions").find(".item_title").attr('title');
    imgItem = $("#addItemInCartOptions").find(".item_img img").attr('src');
    $('#addItemInCompare .item_title').text(titleItem);
    $('#addItemInCompare .item_img img').attr('src', imgItem);
    var ModalName = $('#addItemInCompare');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName)

    $.get(href + '&ajax_compare=1&backurl=' + decodeURIComponent(window.location.pathname), function (data) {
            var compare = $("#compare");
            compare.html(data);

            if (compare.css("display") == "none") {
                compare.css({"display": "block", "height": "0"});
            }
        }
    );
    return false;
}

function addHtml(lastPropCode, arSKU, mode, type) {
    if (mode == "list") {
        if (type == "clear_cart" || type == "clear_compare") {
            BX("listItemPrice").innerHTML = item_price;
            if (type == "clear_cart")
                BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart" >' + BX.message('addToCart') + '</a>';
            else if (type == "clear_compare")
                BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart">' + BX.message('addCompare') + '</a>';
            return;
        }
        var selectedSkuId = BX(lastPropCode).value;
        for (var i = 0; i < arSKU.length; i++) {
            if (arSKU[i]["ID"] == selectedSkuId) {
                var price_block = "";
                for (var price_code in arSKU[i]["PRICES"]) {
                    if (arSKU[i]["PRICES"][price_code]["TITLE"] != "")
                        price_block = price_block + "<p class='price_title'>" + arSKU[i]["PRICES"][price_code]["TITLE"] + "</p>";
                    if (arSKU[i]["PRICES"][price_code]["DISCOUNT_PRICE"] != "") {
                        price_block = price_block + "<div class='discount-price item_price'>" + arSKU[i]["PRICES"][price_code]["DISCOUNT_PRICE"] + "</div>" + "<div class='old-price item_old_price'>" + arSKU[i]["PRICES"][price_code]["PRICE"] + "</div>";
                    }
                    else {
                        price_block = price_block + "<div class='price'>" + arSKU[i]["PRICES"][price_code]["PRICE"] + "</div>";
                    }
                }
                BX("listItemPrice").innerHTML = price_block;

                if (type == "cart") {
                    if (arSKU[i]["CAN_BUY"]) {
                        if (arSKU[i]["CART"] == "")
                            BX("element_buy_button").innerHTML = '<a href="' + arSKU[i]["ADD_URL"] + '" rel="nofollow" class="bt3 addtoCart" onclick=" return addOfferToCart(this, \'list\', \'' + BX.message('inCart') + '\');" id="catalog_add2cart_link_' + arSKU[i]["ID"] + '"><span></span>' + BX.message('addToCart') + '</a>';
                        else
                            BX("element_buy_button").innerHTML = '<a rel="nofollow" class="bt3 addtoCart" onclick=" return addOfferToCart(this, \'list\', \'' + BX.message('inCart') + '\');" id="catalog_add2cart_link_' + arSKU[i]["ID"] + '"><span></span>' + BX.message('addToCart') + '</a>';
                        /*	else if (arSKU[i]["CART"] == "inCart")
                         BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart"  id="catalog_add2cart_link_'+arSKU[i]["ID"]+'">'+BX.message('inCart')+'</a>';//.setAttribute("href",arSKU[i]["ADD_URL"]).innerHTML = '<span class="cartbuy"></span> ';
                         else if (arSKU[i]["CART"] == "delay")
                         BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart"  id="catalog_add2cart_link_'+arSKU[i]["ID"]+'">'+BX.message('delayCart')+'</a>';*/
                    }
                    else {
                        if (arSKU[i]["CART"] == "inSubscribe")
                            BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart">' + BX.message('inSubscribe') + '</a>';
                        else if (BX.message["USER_ID"] > 0 && arSKU[i]["SUBSCRIBE_URL"] != "")
                            BX("element_buy_button").innerHTML = '<a href="' + arSKU[i]["SUBSCRIBE_URL"] + '" rel="nofollow" class="bt3" onclick="return addOfferToSubscribe(this, \'' + BX.message('inSubscribe') + '\', \'list\');" id="catalog_add2cart_link_"' + arSKU[i]["ID"] + '">' + BX.message('subscribe') + '</a>';
                        else
                            BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3" onclick="showAuthForSubscribe(this, \'' + arSKU[i]["ID"] + '\', \'' + arSKU[i]["SUBSCRIBE_URL"] + '\')">' + BX.message('subscribe') + '</a>';
                    }
                }
                else if (type == "compare" && BX("element_buy_button")) {
                    if (arSKU[i]["COMPARE"] == "inCompare")
                        BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3">' + BX.message('inCompare') + '</a>';
                    else
                        BX("element_buy_button").innerHTML = '<a href="' + arSKU[i]["COMPARE_URL"] + '" rel="nofollow" class="bt3 addtoCompare" onclick="return addOfferToCompare(this, \'list\', \'' + BX.message('inCompare') + '\');" id="catalog_add2compare_link_' + arSKU[i]["ID"] + '">' + BX.message('addCompare') + '</a>';

                }
                break;
            }
        }
    }
    else if (mode == "detail") {
        if (type == "clear_cart") {
            BX("minOfferPrice").style.display = "block";
            BX("currentOfferPrice").innerHTML = "";
            BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart">' + BX.message('addToCart') + '</a><br/><br/><br/>';
            if (BX("element_compare_button"))
                BX("element_compare_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart">' + BX.message('addCompare') + '</a>';
            return;
        }
        var selectedSkuId = BX(lastPropCode).value;
        for (var i = 0; i < arSKU.length; i++) {
            if (arSKU[i]["ID"] == selectedSkuId) {
                BX("minOfferPrice").style.display = "none";

                var price_block = "";
                for (var price_code in arSKU[i]["PRICES"]) {
                    if (arSKU[i]["PRICES"][price_code]["TITLE"] != "")
                        price_block = price_block + "<br>" + arSKU[i]["PRICES"][price_code]["TITLE"];
                    if (arSKU[i]["PRICES"][price_code]["DISCOUNT_PRICE"] != "") {
                        price_block = price_block + "<div class='discount-price item_price'>" + arSKU[i]["PRICES"][price_code]["DISCOUNT_PRICE"] + "</div>" + "<div class='old-price item_old_price'>" + arSKU[i]["PRICES"][price_code]["PRICE"] + "</div>";
                    }
                    else {
                        price_block = price_block + "<div class='price'>" + arSKU[i]["PRICES"][price_code]["PRICE"] + "</div>";
                    }
                }
                BX("currentOfferPrice").innerHTML = price_block;

                if (arSKU[i]["CAN_BUY"]) {
                    if (arSKU[i]["CART"] == "")
                        BX("element_buy_button").innerHTML = '<a href="' + arSKU[i]["ADD_URL"] + '" rel="nofollow" class="bt3 addtoCart" onclick="arSKU[' + i + '][\'CART\']= \'inCart\'; return addToCart(this, \'detail\', \'' + BX.message('inCart') + '\', \'cart\'); " id="catalog_add2cart_link_' + arSKU[i]["ID"] + '"><span></span>' + BX.message('addToCart') + '</a><br/><br/><br/>';
                    else
                        BX("element_buy_button").innerHTML = '<a rel="nofollow" class="bt3 addtoCart" onclick="arSKU[' + i + '][\'CART\']= \'inCart\'; return addToCart(this, \'detail\', \'' + BX.message('inCart') + '\', \'cart\'); " id="catalog_add2cart_link_' + arSKU[i]["ID"] + '"><span></span>' + BX.message('addToCart') + '</a><br/><br/><br/>';
                    /*	else if (arSKU[i]["CART"] == "inCart")
                     BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart"  id="catalog_add2cart_link_'+arSKU[i]["ID"]+'">'+BX.message('inCart')+'</a><br/><br/><br/>';
                     else if (arSKU[i]["CART"] == "delay")
                     BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt3 incart"  id="catalog_add2cart_link_'+arSKU[i]["ID"]+'">'+BX.message('delayCart')+'</a><br/><br/><br/>';*/
                }
                else {
                    if (arSKU[i]["CART"] == "inSubscribe")
                        BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt2 incart">' + BX.message('inSubscribe') + '</a><br/><br/><br/>';
                    else if (BX.message["USER_ID"] > 0 && arSKU[i]["SUBSCRIBE_URL"] != "")
                        BX("element_buy_button").innerHTML = '<a href="' + arSKU[i]["SUBSCRIBE_URL"] + '" rel="nofollow" onclick="return addOfferToSubscribe(this, \'' + BX.message('inSubscribe') + '\', \'detail\');" class="bt2" id="catalog_add2cart_link_"' + arSKU[i]["ID"] + '">' + BX.message('subscribe') + '</a><br/><br/><br/>';
                    else
                        BX("element_buy_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt2" onclick="showAuthForSubscribe(this, \'' + arSKU[i]["ID"] + '\', \'' + arSKU[i]["SUBSCRIBE_URL"] + '\')">' + BX.message('subscribe') + '</a><br/><br/><br/>';

                }
                if (BX("element_compare_button"))
                    if (arSKU[i]["COMPARE"] == "inCompare")
                        BX("element_compare_button").innerHTML = '<a href="javascript:void(0)" rel="nofollow" class="bt2">' + BX.message('inCompare') + '</a>';
                    else
                        BX("element_compare_button").innerHTML = '<a href="' + arSKU[i]["COMPARE_URL"] + '" rel="nofollow" class="bt2 addtoCompare" onclick="arSKU[' + i + '][\'COMPARE\']= \'inCompare\'; return addToCompare(this, \'detail\', \'' + BX.message('inCompare') + '\');" id="catalog_add2compare_link_' + arSKU[i]["ID"] + '">' + BX.message('addCompare') + '</a>';
                break;
            }
        }
    }
}

function checkSKU(form_name, SKU, prop_num, arProperties) {
    for (var i = 0; i < prop_num; i++) {
        if (SKU[i] != document.forms[form_name][arProperties[i].CODE].value)
            return false;
    }
    return true;
}

function buildSelect(form_name, cont_name, prop_num, arSKU, arProperties, mode, type) {
    var properties_num = arProperties.length;
    var lastPropCode = arProperties[properties_num - 1].CODE;

    for (var i = prop_num; i < properties_num; i++) {
        var q = BX('prop_' + i);
        if (q)
            q.parentNode.removeChild(q);
    }

    var select = BX.create('SELECT', {
        props: {
            name: arProperties[prop_num].CODE,
            id: arProperties[prop_num].CODE
        },
        events: {
            change: (prop_num < properties_num - 1)
                ? function () {
                    buildSelect(form_name, cont_name, prop_num + 1, arSKU, arProperties, mode, type);
                    if (this.value != "null") BX(arProperties[prop_num + 1].CODE).disabled = false;
                    addHtml(lastPropCode, arSKU, mode, "clear_" + type);
                }
                : function () {
                    if (this.value != "null")
                        addHtml(lastPropCode, arSKU, mode, type);
                    else
                        addHtml(lastPropCode, arSKU, mode, "clear_" + type);
                }
        }
    });
    if (prop_num != 0) select.disabled = true;

    var ar = [];
    select.add(new Option('--' + BX.message("chooseProp") + ' ' + arProperties[prop_num].NAME + '--', 'null'));
    for (var i = 0; i < arSKU.length; i++) {
        if (checkSKU(form_name, arSKU[i], prop_num, arProperties) && !BX.util.in_array(arSKU[i][prop_num], ar)) {
            select.add(new Option(
                arSKU[i][prop_num],     //text
                prop_num < properties_num - 1 ? arSKU[i][prop_num] : arSKU[i]["ID"]// value

            ));
            ar.push(arSKU[i][prop_num]);
        }
    }

    var cont = BX.create('tr', {
        props: {id: 'prop_' + prop_num},
        children: [
            BX.create('td', {html: arProperties[prop_num].NAME + ': '}),
            BX.create('td', {
                children: [
                    select
                ]
            }),
        ]
    });

    var tmp = BX.findChild(BX(cont_name), {tagName: 'tbody'}, false, false);
    tmp.appendChild(cont);

    if (prop_num < properties_num - 1)
        buildSelect(form_name, cont_name, prop_num + 1, arSKU, arProperties, mode, type);
}

function showOfferPopup(element, mode, text, arSKU, arProperties, mess, type) {
    if (!element || !element.href)
        return;
    BX.message(mess);
    var button = $(element);
    if (mode == "detail")
        button.unbind('click');
    else if (mode == "list")
        button.unbind('click');

    var titleItem = button.parents(".R2D2").find(".item_title").attr('title')
    var imgItem = button.parents(".R2D2").find(".item_img").attr('src');
    var item_price = button.parents(".R2D2").find(".item_price").text();
    var item_old_price = button.parents(".R2D2").find(".item_old_price").text();
    $('#addItemInCartOptions .item_title').text(titleItem);
    $('#addItemInCartOptions .item_title').attr('title', titleItem);
    $('#addItemInCartOptions .item_price').text(item_price);
    $('#addItemInCartOptions .item_old_price').text(item_old_price);
    $('#addItemInCartOptions .item_img img').attr('src', imgItem);
    $("#addItemInCartOptions .item_count").attr('value', 1);
    $("#sku_selectors_list tbody").html("");
    var ModalName = $('#addItemInCartOptions');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName)
    window.item_price = item_price;

    buildSelect("buy_form_list", "sku_selectors_list", 0, arSKU, arProperties, mode, type);
    var properties_num = arProperties.length;
    var lastPropCode = arProperties[properties_num - 1].CODE;
    addHtml(lastPropCode, arSKU, mode, "clear_" + type);

    return false;
}

function setEqualHeight(columns) {
    var tallestcolumn = 0;
    columns.each(function () {
        currentHeight = $(this).height();
        if (currentHeight > tallestcolumn) {
            tallestcolumn = currentHeight;
        }
    });
    columns.height(tallestcolumn);
}

function setEqualHeight2(columns) {
    var tallestcolumn = 0;
    columns.each(function () {
        currentHeight = $(this).height();
        if (currentHeight > tallestcolumn) {
            tallestcolumn = currentHeight;
        }
    });
    columns.height(tallestcolumn);
}

function setEqualHeight3(columns) {
    var tallestcolumn = 0;
    columns.each(function () {
        currentHeight = $(this).height();
        if (currentHeight > tallestcolumn) {
            tallestcolumn = currentHeight;
        }
    });
    columns.height(tallestcolumn);
}

$(".horizontalfilter > li > span").live('click', function () {
    var ind = $(this).parent("li").index();
    ind++;
    if ($(this).parent("li").hasClass("active")) {
    } else {
        $(this).parents('.filtren.compare').find('.active').removeClass('active')
        $(this).parent("li").addClass('active');
        $(".filtren.compare .cntf").find(".cnt:nth-child(" + ind + ")").addClass('active');
    }
    return false;
});
/*TABS*/
/* */
$(".tabsblock > .tabs > a").live('click', function () {
    var ind = $(this).index();
    ind++;
    if ($(this).hasClass("active")) {
    } else {
        $(this).parents('.tabsblock').find('.active').removeClass('active')
        $(this).addClass('active');
        $(".tabsblock").find(".cnt:nth-child(" + ind + ")").addClass('active');
    }
    return false;
});

$("#notify_auth_form > .social > form > ul > li > a").live('click', function () {
    setTimeout(function () {
        var modalH = $("#popupFormSubscribe").height();
        var modalW = $("#popupFormSubscribe").width();
        $("#popupFormSubscribe").animate({
            "margin-left": "-" + (parseInt(modalW) / 2) + "px",
            "margin-top": "-" + (parseInt(modalH) / 2) + "px"
        }, 300)
    }, 100)
});
$("#login > .social ul li a").live('click', function () {
    setTimeout(function () {
        var modalH = $("#login").height();
        var modalW = $("#login").width();
        $("#login").animate({
            "margin-left": "-" + (parseInt(modalW) / 2) + "px",
            "margin-top": "-" + (parseInt(modalH) / 2) + "px"
        }, 300)

    }, 100)
});

$(document).ready(function () {
    $(".tlistitem_horizontal_shadow").css({"width": $(".tlistitem_horizontal_shadow").parent(".R2D2").width() + "px"})
    setEqualHeight($(".listitem li > h4"));
    setEqualHeight2($("#foo_newproduct > li > h4"));
    setEqualHeight3($("#foo_saleleader > li > h4"));
    setEqualHeight($(".listitem li > .buy"));
    setEqualHeight2($("#foo_newproduct > li > .buy"));
    setEqualHeight3($("#foo_saleleader > li > .buy"));
    $(".caroufredsel_wrapper").css({"margin": "0 auto"});
    $(".slideViewer").css({"float": "none"});

    var counttd = $(".table_compare table tr:first").find("td img").size()
    if (counttd < 4) {
        compareTable(counttd);
    }
    $(".table_compare table").find("tr td:nth(0)").css("width", "160px")
    $(".table_compare table").find("tr:nth(0) td").css("height", 120 + "px")
    $(".table_compare table").find("tr:nth(1) td").css("height", $(".table_compare table").find("tr:nth(1) td").height() + "px")
    $(".table_compare").css("height", $(".table_compare").find("table").height() + 30 + "px")
    var countli = $(".caroufredsel_wrapper ul li").size()
    if (countli < 4) {
        $(".caroufredsel_wrapper").find(".next").addClass("disable")
    }
});

function addProductToSubscribe(element, url, id) {
    BX.showWait();
    if ($('#url_notify_' + id))
        $('#url_notify_' + id).html('').addClass("incart");

    var titleItem = $(element).parents(".R2D2").find(".item_title").attr('title');
    if (titleItem) {
        var imgItem = $(element).parents(".R2D2").find(".item_img").attr('src');
    }
    else {
        titleItem = $("#addItemInCartOptions").find(".item_title").attr('title');
        imgItem = $("#addItemInCartOptions").find(".item_img img").attr('src');
    }

    BX.ajax.post(url, '', function (res) {
        BX.closeWait();
        document.body.innerHTML = res;
        $('#addItemInSubscribe .item_title').text(titleItem);
        $('#addItemInSubscribe .item_img img').attr('src', imgItem);
        var modalH = $('#addItemInSubscribe').height();
        $("#bgmod").css("display", "block");
        $('#addItemInSubscribe').css({"display": "block", "margin-top": "-" + (parseInt(modalH) / 2) + "px"});
    });
}

function showAuthForSubscribe(element, id, notify_url) {
    /* $("#popupFormSubscribe").css({display: "block"}); */
    var ModalName = $('#popupFormSubscribe');
    CentriredModalWindow(ModalName);
    OpenModalWindow(ModalName);
    window.button = $(element);
    window.subId = id;
    BX('popup_notify_url').value = notify_url;
    //BX('popup_user_email').focus();
}

function showAuthForm() {
    if (BX('notify_auth_form').style.display == "none") {
        BX('notify_auth_form').style["display"] = "block";
        BX('notify_user_auth').value = 'Y';
        BX('notify_user_email').style["display"] = "none";
        BX('subscribeBackButton').style["display"] = "inline";
        BX('subscribeCancelButton').style["display"] = "none";
        $('#popup_n_error').css('display', 'none');

        var ModalName = $('#popupFormSubscribe');
        $(ModalName).css({'width': 'auto'})
        /*	 	var modalW = $(ModalName).width();
         $(ModalName).css({"margin-left":"-"+(parseInt(modalW)/2)+"px","opacity":0}) */
        CentriredModalWindow(ModalName);
        OpenModalWindow(ModalName);
    }
}

function showUserEmail() {
    if (BX('notify_user_email').style.display == "none") {
        BX('notify_user_email').style["display"] = "block";
        BX('notify_user_auth').value = 'N';
        BX('notify_auth_form').style["display"] = "none";
        BX('subscribeBackButton').style["display"] = "none";
        BX('subscribeCancelButton').style["display"] = "inline";
        $('#popup_n_error').css('display', 'none');

        var ModalName = $('#popupFormSubscribe');
        $(ModalName).css({'width': 'auto'})
        /* 		var modalW = $(ModalName).width();
         $(ModalName).css({"margin-left":"-"+(parseInt(modalW)/2)+"px","opacity":0}) */
        CentriredModalWindow(ModalName);
        OpenModalWindow(ModalName);
    }
}

function gamburger_Menu(d_this) {
    if ($(d_this).hasClass("bgreen")) {
        /*$(d_this).removeClass("bgreen");*/
        /*$("#tpl_menu").toggle('slow');*/
        $(".header-brandzone-nav").toggle('slow');
    } else {
        /*$(d_this).addClass("bgreen");*/
        /*$("#tpl_menu").toggle('slow');*/
        $(".header-brandzone-nav").toggle('slow');
    }

    function ss_menu_pos() {
        var pos = $("#tpl_menu #top-menu .content-inner .large_new li.lili.selected a").position().top;
        $("#tpl_menu .header-brandzone-nav #top-menu-layout").scrollTop(pos);
    }

    setTimeout(ss_menu_pos, 300);
}

function open_serch_top(d_this) {
    if ($(d_this).hasClass("bgreen")) {
        $(d_this).removeClass("bgreen");
        $(".c4").css('height', '0px');
    } else {
        $(d_this).addClass("bgreen");
        $(".c4").css('height', '60px');
    }
}

function AjaxQuanitys(val, href) {
    var idd = $("#QUA_VAL").val();
    BX.ajax.post(href, {'idd': idd, 'val': val}, function (res) {
        console.log(res);
        if (res == "Y") {
            $('#addItemInCart #detaili_bsket').load('/ajax/detaili_bsket.php?ELEMENT_ID=all');
            $.get('/ajax/detaili_bsket.php?ELEMENT_ID=' + idd, function (data) {
                $('#addItemInCart #QUANTITY_').val(data);
            });
            $("#cart_line").load("/index.php #cart_line > *");
        } else if (res != "N") {
            $('#addItemInCart #QUANTITY_').val(res);
        }
    });
}

$(document).on('click', '.serchiis', function () {
    if ($(".c4").hasClass("opencategoriserch")) {
        $(".c4, .c2").removeClass('opencategoriserch');
    } else {
        $(".c4, .c2").addClass('opencategoriserch');
    }
});
// A-Z
$(document).ready(function () {
    $('input[name="agree"], input[name="agree2"]').click(function () {

        if ($('input[name="agree"]').prop("checked") && $('input[name="agree2"]').prop("checked")) {
            console.log('agree OK');
            var clic = $('#ORDER_CONFIRM_BUTTON').data("clic");
            //console.log('clic',clic);
            $('#ORDER_CONFIRM_BUTTON').removeClass("disabled").attr("onclick", clic);

        } else {
            console.log('agree NOT');
            var noclic = $('#ORDER_CONFIRM_BUTTON').data("noclic");
            //console.log('noclic',noclic);
            $('#ORDER_CONFIRM_BUTTON').addClass("disabled").attr("onclick", noclic);
        }
    });
});

$(document).ready(function () {
    $("#soa-property-3").mask("+7 (999) 999-99-99");
});

$(document).ready(function () {
    $("#owl-2, #owl-3, #owl-4").owlCarousel({
        items: 4,
        itemsDesktop: [1263, 3],
        itemsTablet: [1024, 2],
        itemsTabletSmall: [768, 2],
        itemsMobile: [480, 1],
        lazyLoad: true,
        pagination: false,
        navigation: true
    });
    $("#owl-1, #owl, #owl-16, #owl-36").owlCarousel({
        items: 4,
        itemsDesktop: [1263, 3],
        itemsTablet: [1024, 2],
        itemsTabletSmall: [768, 2],
        itemsMobile: [480, 1],
        lazyLoad: true,
        pagination: false,
        navigation: true
    });
    $("#owl_slide_n").owlCarousel({
        items: 1,
        itemsDesktop: [1263, 1],
        itemsTablet: [1024, 1],
        itemsTabletSmall: [768, 1],
        itemsMobile: [480, 1],
        lazyLoad: true,
        pagination: true,
        navigation: false,
        autoPlay: true
    });

    jQuery(function () {
        $().UItoTop();
    });
});

jQuery(function ($) {
    $(".phone_mask").mask("+7 (999) 999-99-99", {
        completed: function () {
            $(this).attr('value', this.val())
        }
    });
    $(".phone_mask2").mask("+7 (999) 999-99-99", {
        completed: function () {/*$(this).attr('value',this.val())*/
        }
    });
});

$(document).ready(function () {
    var maxCount = 2000;
    $("#counter").html(maxCount);
    $("#vopr").keyup(function () {
        var revText = this.value.length;
        if (this.value.length > maxCount) {
            this.value = this.value.substr(0, maxCount);
        }
        var cnt = (maxCount - revText);
        if (cnt <= 0) {
            $("#counter").html('0');
        }
        else {
            $("#counter").html(cnt);
        }
    });

    $("div.d_image a[rel^='prettyPhoto']").prettyPhoto({
        theme: 'dark_rounded'
    });

    $("div.d_image a[data-pretty^='prettyPhoto']").prettyPhoto({
        theme: 'dark_rounded'
    });

    markFavorites();
});


function validate() {
    var x = document.forms["vopros"]["name"].value;
    var y = document.forms["vopros"]["email"].value;
    var error = false;

    if (x.length == 0) {
        document.getElementById("namef").innerHTML = "���� ���� ��� ����������� ��� ����������";
        error = true;
    } else {
        document.getElementById("namef").innerHTML = "";
    }

    //���� ���� email ������ ������� ��������� � ������������ �������� �����
    if (y.length == 0) {
        document.getElementById("emailf").innerHTML = "���� ��� e-mail ����������� ��� ����������";
        error = true;
    } else {
        document.getElementById("emailf").innerHTML = "";

        //�������� �������� �� �������� ��������� � ���� email ������� @ � .
        at = y.indexOf("@");
        dot = y.indexOf(".");

        //���� ���� �� �������� ��� ������� ���� email ������ �� �����
        if (at < 1 || dot < 1) {
            document.getElementById("emailf").innerHTML = "��� e-mail ������ �� ���������";
            error = true;
        }
    }
    if (!error) {
        return true;
    } else {
        return false;
    }

}