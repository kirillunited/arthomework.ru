$(document).ready(function() {
	$('.catalog-detail-images').fancybox({
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'speedIn': 600,
		'speedOut': 200,
		'overlayShow': false,
		'cyclic' : true,
		'padding': 20,
		'titlePosition': 'over',
		'onComplete': function() {
		$("#fancybox-title").css({ 'top': '100%', 'bottom': 'auto' });
		}
	});
	
	/*$('#subscrible_ajax').click(function(){
		$(".subscribe").load("/ajax/subscrible.php","id="+$('#subscrible_ajax').attr("rel"));
	});*/
});
function plash(){
	if( $("div.po_plash").hasClass('show') ){
		$("div.po_plash").removeClass("show").empty();
	}else{
		$("div.po_plash").load('/catalog/geoip_detail.php').addClass("show");
	}	
}

function open_tab(tab, d_this){
	$(".left_puncts div").removeClass("active");
	$(d_this).addClass("active");
	
	$(".right_puncts > div").css("display","none");
	$(".right_puncts > div.tab_"+tab).css("display","block");
}

function minus(){
	var val = $(".d_quan input").attr("value");
	val = val - 1;
	if (val < 1)
		val = 1;
	$(".d_quan input").attr("value",val);
}

function plus(){
	var val = $(".d_quan input").attr("value");
	val = parseFloat(val) + 1;
	$(".d_quan input").attr("value",val);
}

$('#btn_city').on('click', function () {
	$('#sbros_ip').click();
});

$('.delivery-info-title').on('click', function () {
	$(this).parent().toggleClass('collapsed');
});

$('div.soc_icons a img').hover(function() {
	var hovSrc = $(this).attr('data-src-hover');
	var currSrc = $(this).attr('src');
    $(this).attr('data-src-hover', currSrc);
    $(this).attr('src', hovSrc);
}, function() {
    var hovSrc = $(this).attr('data-src-hover');
    var currSrc = $(this).attr('src');
    $(this).attr('data-src-hover', currSrc);
    $(this).attr('src', hovSrc);
});