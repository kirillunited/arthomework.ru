$(document).ready(function(){
	
$("div.d_image a[rel^='prettyPhoto']").prettyPhoto({
  	theme: 'dark_rounded'
  });
});

function ecomerck(id,name,price,quantity,brand,category, position){
window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "ecommerce": {
        "add": {
            "products": [
                {
                    "id": id,
                    "name": name,
                    "price": price,
                    "quantity": quantity,
					"brand": brand,
					"category": category,
					"position": position
                }
            ]
        }
    }
});
}
