function open_submenu(d_this) {
	if (! $(d_this).hasClass("anim_rot_to_top")){
		$(d_this).addClass("anim_rot_to_top");
		$(d_this).removeClass("anim_rot_to_bottom");
	}else{
		$(d_this).removeClass("anim_rot_to_top");
		$(d_this).addClass("anim_rot_to_bottom");
	}
	$(d_this).parent().parent().find(".submenu").toggle("slow");
}

function close_menu(d_this) {
	$(".header-brandzone-nav").toggle('slow');
}